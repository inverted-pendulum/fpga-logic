#include "Vuart_m.h"
#include "verilated.h"
#include "verilated_vcd_c.h"
#include <iostream>

int main(int argc, char* argv[]) {
    Verilated::commandArgs(argc, argv);
    Verilated::traceEverOn(true);

    Vuart_m top;
    VerilatedVcdC trace;
    top.trace(&trace, 99);
    trace.open("verilator_out.vcd");

    int i_clk = 1;
    int i = 0;

    for (size_t j = 0; j < 500000; j++) {
        i_clk = 1 - i_clk;
        top.i_clk = i_clk;
        top.eval();

        trace.dump(i++);
        trace.flush();

        if (j == 0 || j == 13) {
            top.i_send = 1;
        } else if (j == 300000) {
            top.i_send = 0;
        }
    }

    trace.flush();
    trace.close();

    return 0;
}
