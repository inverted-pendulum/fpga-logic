PROJ_NAME = uart_m

all: $(PROJ_NAME).v $(PROJ_NAME).cpp
	verilator -Wall --trace --cc $(PROJ_NAME).v --exe $(PROJ_NAME).cpp
	make -C obj_dir -f V$(PROJ_NAME).mk -j4

.PHONY = clean show

show:
	./obj_dir/V${PROJ_NAME}
	gtkwave ./verilator_out.vcd ./save.gtkw

clean:
	rm -rf ./obj_dir
	rm -rf ./verilated.d
	rm -rf ./verilator_out.vcd
