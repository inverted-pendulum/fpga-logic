module slow_clock 
    #(parameter FREQ_SCALE=1) 
(
    input wire i_clk,
    output wire o_slow_clock
);

reg r_slow_clock;
integer r_counter = 0;

assign o_slow_clock = r_slow_clock;

always @(posedge i_clk)
begin
    if(r_counter == FREQ_SCALE - 1) 
    begin
        r_counter <= 0;
        r_slow_clock <= ~r_slow_clock;
    end 
    else
    begin
        r_counter <= r_counter + 1;
    end
end

endmodule
