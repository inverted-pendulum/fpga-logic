module uart_m(
    input wire i_clk,
    input wire i_send,
    //input reg[7:0] i_data,
    output reg o_data
);
    parameter STATE_IDLE = 0;
    parameter STATE_DATA = 1;
    parameter STATE_PARITY = 2;
    parameter STATE_END = 3;

    parameter START_BIT = 0;
    parameter PARITY_BIT = 1;
    parameter END_BIT = 1;
    parameter DATA_SIZE = 8;

    reg[7:0] i_data = 98;
    reg[2:0] r_state = STATE_IDLE;
    reg[2:0] r_curr_bit;
    wire r_uart_clk;
    
    initial 
    begin
        o_data = 1;
    end

    // baud 9600 -> 9.6 KHz -> scale = 12 MHz / (2 * 9.6) KHz
    slow_clock #(.FREQ_SCALE(12000000 / (115200 * 2))) m_uart_clk(
        .i_clk(i_clk),
        .o_slow_clock(r_uart_clk)
    );

    always @(posedge r_uart_clk)
    begin
        if (i_send == 0)
        begin
            o_data <= 1;
        end

        // STATE IDLE
        if (i_send == 1 && r_state == STATE_IDLE)
        begin
            o_data <= START_BIT;
            r_state <= STATE_DATA;
            r_curr_bit <= 0;
        end

        // STATE DATA
        if (r_state == STATE_DATA)
        begin
            o_data <= i_data[r_curr_bit];
            r_curr_bit <= r_curr_bit + 1;

            // all data was sent
            if (r_curr_bit + 1 == DATA_SIZE)
            begin
                r_state <= STATE_END;
            end
        end

        // STATE PARITY
        if (r_state == STATE_PARITY) 
        begin
            o_data <= PARITY_BIT;
            r_state <= STATE_END;
        end

        // STATE END
        if (r_state == STATE_END) 
        begin
            o_data <= END_BIT;
            r_state <= STATE_IDLE;
        end
    end

endmodule
